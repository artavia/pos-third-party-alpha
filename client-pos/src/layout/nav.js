import React from "react";
import { NavLink } from 'react-router-dom';

const Nav = () => {
  
  const noOpLink = (event) => { event.preventDefault(); };

  let element = (

    <nav className="navbar navbar-expand-md navbar-dark bg-dark" aria-label="Fourth navbar example">
      <div className="container-fluid">

        <NavLink className="navbar-brand" to="/" onClick={ noOpLink }>Real Time POS System</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarsExample04">
          <ul className="navbar-nav me-auto mb-2 mb-md-0">
            
            <li className="nav-item">
              <NavLink className="nav-link" to={"/"} activeclassname={"active"} end>
                Index (POS)
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to={"/inventory"} activeclassname={"active"} end>
                Inventory
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to={"/transactions"} activeclassname={"active"} end>
                Transactions
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to={"/livecart"} activeclassname={"active"} end>
                LiveCart
              </NavLink>
            </li>

          </ul>          
        </div>
      </div>
    </nav>

  );
  return element;
};

export {Nav};