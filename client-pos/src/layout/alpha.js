import React from "react";

// import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'; // v5
import { BrowserRouter, Routes, Route } from "react-router-dom"; // v6
// import { Navigate } from "react-router-dom"; // v6

import { Main } from "./main";
import { DNEPage } from '../components/pages/DNEPage';
import { Inventory } from '../components/pages/Inventory';
import { Pos } from '../components/pages/Pos';
import { Transactions } from '../components/pages/Transactions';
import { LiveCart } from '../components/pages/LiveCart';
import { ViewTransaction } from '../components/pages/ViewTransaction';
import { ViewProduct } from '../components/pages/ViewProduct';

const Alpha = () => {
  const element = (
    <BrowserRouter>
      <Main>
        <Routes>          
          <Route path="/" element={ <Pos/> } />

          <Route path="/inventory" element={ <Inventory/> } />
          <Route path="/transactions" element={ <Transactions/> } />
          <Route path="/livecart" element={ <LiveCart/> } />

          <Route path="/transaction/:id" element={ <ViewTransaction/> }  />
          <Route path="/product/:id" element={ <ViewProduct/> }  />

          <Route path="*" element={ <DNEPage /> } />

        </Routes>
      </Main>
    </BrowserRouter>
  );
  return element;
};

export {Alpha};