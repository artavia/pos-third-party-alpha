import React, { useState, useRef } from 'react';
import { ProductViewer } from '../facades/productviewer';

const Inventory = () => { 

  let snackbar = useRef(null);

  const [ products, setProducts ] = useState( [] );
  const [ snackMessage, setSnackMessage ] = useState( "" );

  const productViewerProperties = {
    
    productsProperties : {
      products: products , 
      setProducts: setProducts , 
    } ,

    snackbarProperties: {
      snackbar: snackbar , 
      snackMessage: snackMessage , 
      setSnackMessage: setSnackMessage ,
    } ,
  };

  let element = ( <ProductViewer { ...productViewerProperties } /> );
  return element;
};

export {Inventory};