import React from 'react';
import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

import { TransactionDetail } from './TransactionDetail';

const CompleteTransactions = (props) => {

  // console.log( "props", props );
  let [ transactionModal, setTransactionModal ] = useState(false);

  const customHideTransactionModal = () => {
    setTransactionModal(false);
  };
  
  const customShowTransactionModal = () => {
    setTransactionModal(true);
  };

  const renderQuantity = (items) => {
    let totalquantity = 0;
    for( var i = 0; i < items.length; i++){
      // console.log( "typeof items[i].quantity" , typeof items[i].quantity ); // number
      totalquantity = totalquantity + items[i].quantity;
    }
    return totalquantity;
  };

  const mapTransactionDetail = ( item, idx, arr ) => {
    var newProps = {
      key: idx , 
      item: item , 
    };
    return <TransactionDetail { ...newProps } />;
  };

  const renderTransactionItems = () => {
    return props.transaction.items.map( mapTransactionDetail );
  };
  
  // console.log( "props.transaction", props.transaction );  

  let element = (
    <>
      <tr>
        <td>{props.transaction._id}</td>
        <td>{props.transaction.date}</td>
        <td>${props.transaction.total}</td>
        <td>${props.transaction.tax}</td>
        <td>{renderQuantity(props.transaction.items)}</td>
        <td>
          <Button variant="info" onClick={customShowTransactionModal}>
            <i className="fas fa-window-restore"></i>
          </Button>
        </td>
        <td>
          <Modal show={ transactionModal } onHide={customHideTransactionModal}>
            <Modal.Header closeButton>
              <Modal.Title>Transaction Details</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="card text-white bg-primary mb-3" /* style={ { "maxWidth": "18rem" } } */ >
                <div className="card-header text-center lead">{props.transaction.date}</div>
                <div className="card-body">
                  <table className='receipt table table-responsive table-striped table-hover'>
                    <thead>
                      <tr className="small">
                        <th className='text-center'>Quantity</th>
                        <th className='text-center'>Product Name</th>
                        <th className='text-center'>Subtotal</th>
                      </tr>
                    </thead>
                    <tbody>
                      { renderTransactionItems() }
                      <tr className="total">
                        <td colSpan={'3'} className='text-center' >Net Total: ${props.transaction.total} </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="primary" onClick={customHideTransactionModal}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
        </td>
      </tr>
    </>
  );
  return element;
};

export {CompleteTransactions};