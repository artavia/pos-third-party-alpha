# Point of Sale (3rd party &quot;Alpha&quot;)- Client

## Description
A simulated point of sale application that uses ExpressJS, NeDB and ReactJS.

## Attribution
This is a project based on the work originally performed by Mr. Krissanawat Kaewsanmuang who published the same **(incomplete)** work at both [Plain English](https://javascript.plainenglish.io/building-a-point-of-sale-system-with-node-react-c2c0395ccaca) and [Hackernoon](https://hackernoon.com/building-simple-point-of-sale-system-with-node-js-react-js-a0e51059ba33). Between applying for jobs (and being rejected for jobs), distractions at home, dentist appointments and other things that life throws at you this took way longer than I anticipated originally. The difference my project and his project is that **mine works**!

## Processing and/or completion date(s)
The [journal](https://gitlab.com/artavia/pos-third-party-alpha/-/blob/main/supporting_material/client-pos-JOURNAL.md) detailing processing dates is set apart due to it's monotonous nature.

## Purpose behind this project
The [background and justification](https://gitlab.com/artavia/pos-third-party-alpha/-/blob/main/supporting_material/BACKGROUND.md) details the rationale behind this project.

## Instructions
In order to operate the development environment, you will have **two terminals** open (front and back respectively). 

On the server side, adjust the NODE_ENV in the .env file and use the appropriate script preset. I prefer **yarn** but **I used npm**. Run **npm install** in the server folder, then, you're off!

On the client side, just run &quot;**npm run start**&quot; as you would normally do. Make sure to first run **npm install** in the client folder, then, you can launch the front-end, too!

Subsequent to that, I did not pop out a **build** with **Create React App** in this project. For the love of God, it uses something called **NeDB**. 

But with the subsequent, accompanying MySQL version of this project, I did pop out a **build** and worked out all of the kinks that were mainly attributed to **Helmet's Content Security Policy**. 

The beef I have with **NeDB** is that CRUD is not CRUD but something else. If you &quot;**update**&quot; a record, it creates another identical record that is read after the original record. And, if you &quot;**delete**&quot; a record, the record is not deleted but obfuscated enough in order that the record cannot be recalled. It's hardly practical!

## Visit the accompanying project
The accompanying project uses **MySQL** and is published at [gitlab](https://gitlab.com/artavia/mysql-pos-third-party-alpha/). You are free to visit if you would like.

### May Jesus bless you
That is all I have to say for now. I hope to have influenced you in some positive manner today. God bless.