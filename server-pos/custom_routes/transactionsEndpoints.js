const express = require("express");
const transactionsRouter = express.Router();
var Datastore = require("nedb");
var Inventory = require("./inventoryEndpoints");
transactionsRouter.use( express.json() );

// Create DB
var transactionsDB = new Datastore( {
  filename: "./custom_db/databases/transactions.db",
  autoload: true
} );

// ## ##################
// ## GET message
// http://127.0.0.1:5000/api/transactions
// ## ##################
transactionsRouter.get( "/", (req,res) => {
  res.send("Transactions API");
} );

// ## ##################
// ## GET all records - transactions
// http://127.0.0.1:5000/api/transactions/all
// ## ##################
transactionsRouter.get( "/all", (req,res) => {
  
  transactionsDB.find( {}, (err,docs) => {
    if(err){
      res.status(500).send(err);
    }
    res.send(docs);
  } );
} );

// ## ##################
// ## GET all records - transactions
// http://127.0.0.1:5000/api/transactions/limit
// http://127.0.0.1:5000/api/transactions/limit?limit=MYFARRGINGLIMIT (?!?!?!)
// ## ##################
transactionsRouter.get( "/limit", (req,res) => {

  var limit = parseInt( req.query.limit, 10 );
  if(!limit){ limit = 5; }

  transactionsDB
  .find( {} )
  .limit(limit)
  .sort( {date: -1} )
  .exec( (err,docs) => {
    if(err){
      res.status(500).send(err);
    }
    res.send(docs);
  } );  
} );

// ## ##################
// FILTERED ARRAY and SORTED IN DESCENDING ORDER RETURNING TRANSACTIONS FOR THE PREVIOUS 24 HOURS
// ## GET transactions for the LAST 24 HOURS - transactions
// http://127.0.0.1:5000/api/transactions/todaysnumbers?date=2022-12-09
// ## ##################
transactionsRouter.get( "/todaysnumbers", (req,res) => {

  // console.log("req.query.date" , req.query.date ); // typeof req.query.date === STRING
  // console.log("typeof req.query.date" , typeof req.query.date ); 
  // console.log("req.query.date.length" , req.query.date.length ); 
  
  // beginning of query date day
  var startNum = new Date(req.query.date).setHours(0,0,0,0); // "typeof x.setHours(...) ... === number
  var startDate = new Date( startNum );
  var startDateUtcMS = startDate.getTime();
  
  // end of query date day
  var endNum = new Date(req.query.date).setHours(23,59,59,999); // "typeof x.setHours(...) ... === number
  var endDate = new Date( endNum );
  var endDateUtcMS = endDate.getTime();

  // console.log("todaysnumbers startDate" , startDate ); 
  // console.log("todaysnumbers endDate" , endDate ); 

  // return all items with date in question
  var searchobject = {
    $where: function () { 
      var focus = new Date( this.date ).getTime();
      return (focus < endDateUtcMS) && (focus > startDateUtcMS);
    } 
  };

  transactionsDB.find( searchobject , async (err,docs) => {
    if(err){
      res.status(500).send(err);
    }
    if(await docs){
      if( await docs.length === 0 ){
        // await console.log("docs", await docs);
        res.send(await docs);
      }
      else
      if( await docs.length > 0 ){
        // await console.log("docs", await docs);
        
        let shallowcopy = [ ...docs ]; // await console.log( "shallowcopy", await shallowcopy );

        const mappedTransactions = await shallowcopy.map( (el, idx, arr) => {
          return { ...el, date: new Date(el.date) }; 
        } ); // await console.log( "mappedTransactions", await mappedTransactions );

        const sortDesc = await mappedTransactions.sort( (a,b) => {
          return new Date( b.date ) - new Date( a.date );
        } ); // await console.log( "sortDesc", await sortDesc );

        const reducedSumOfAllSales = await sortDesc.reduce( ( accumulator, el, idx, arr ) => {
          return accumulator + el.total;
        } , 0 ); // await console.log( "reducedSumOfAllSales", await reducedSumOfAllSales ); // 662.05 

        const presentableTransactions = await sortDesc.map( (el, idx, arr) => { 
          return { ...el, date: `${el.date.toLocaleDateString()} ${el.date.toLocaleTimeString()}` }; 
        } ); // await console.log("presentableTransactions", await presentableTransactions );

        const responseObject = {
          docs: presentableTransactions ,
          numbers: reducedSumOfAllSales 
        };

        res.send(await responseObject);
      }
    }
  } );

} );

// ## ##################
// FILTERED ARRAY and SORTED IN DESCENDING ORDER RETURNING TRANSACTIONS WITHIN A SPECIFIC RANGE OF DATES
// ## GET transactions for a particular date - transactions
// http://127.0.0.1:5000/api/transactions/by-date
// http://127.0.0.1:5000/api/transactions/customrange?startdate=1670112000000&enddate=1670371200000
// ## ##################
transactionsRouter.get( "/customrange", (req,res) => {

  // console.log("req.query.startdate" , req.query.startdate ); // typeof req.query.startdate === STRING
  // console.log("typeof req.query.startdate" , typeof req.query.startdate ); 

  // console.log("req.query.enddate" , req.query.enddate ); // typeof req.query.enddate === STRING
  // console.log("typeof req.query.enddate" , typeof req.query.enddate );

  // if dates are provided
  if( !!(req.query.startdate && req.query.startdate >= 0) && !!(req.query.enddate && req.query.enddate >= 0) ){ 
    
    // beginning of query date day
    var startNum = new Date( parseInt( req.query.startdate , 10 ) ).setHours(0,0,0,0); // "typeof x.setHours(...) ... === number
    var startDate = new Date( startNum );
    var startDateUtcMS = startDate.getTime();
    
    // end of query date day
    var endNum = new Date( parseInt( req.query.enddate , 10 ) ).setHours(23,59,59,999); // "typeof x.setHours(...) ... === number
    var endDate = new Date( endNum );
    var endDateUtcMS = endDate.getTime();
  }

  // return all items with date in question
  var searchobject = {
    $where: function () { 
      var focus = new Date( this.date ).getTime();
      return (focus < endDateUtcMS) && (focus > startDateUtcMS);
    } 
  };

  transactionsDB.find( searchobject , async (err,docs) => {
    if(err){
      res.status(500).send(err);
    }
    if(await docs){
      if( await docs.length === 0 ){
        // await console.log("docs", await docs);
        res.send(await docs);
      }
      else
      if( await docs.length > 0 ){
        // await console.log("docs", await docs);
        
        let shallowcopy = [ ...docs ]; // await console.log( "shallowcopy", await shallowcopy );

        const mappedTransactions = await shallowcopy.map( (el, idx, arr) => {
          return { ...el, date: new Date(el.date) }; 
        } ); // await console.log( "mappedTransactions", await mappedTransactions );

        const sortDesc = await mappedTransactions.sort( (a,b) => {
          return new Date( b.date ) - new Date( a.date );
        } ); // await console.log( "sortDesc", await sortDesc );

        const reducedSumOfAllSales = await sortDesc.reduce( ( accumulator, el, idx, arr ) => {
          return accumulator + el.total;
        } , 0 ); // await console.log( "reducedSumOfAllSales", await reducedSumOfAllSales ); // 662.05 

        const presentableTransactions = await sortDesc.map( (el, idx, arr) => { 
          return { ...el, date: `${el.date.toLocaleDateString()} ${el.date.toLocaleTimeString()}` }; 
        } ); // await console.log("presentableTransactions", await presentableTransactions );

        const responseObject = {
          docs: presentableTransactions ,
          numbers: reducedSumOfAllSales 
        };

        res.send(await responseObject);
      }
    }
  } );

} );

// ## ##################
// ## GET one record - transactions
// http://127.0.0.1:5000/api/transactions/:transactionId
// ## ##################
transactionsRouter.get( "/:transactionId", (req,res) => {

  transactionsDB.find({
    _id: req.params.transactionId
  } , (err,doc) => {
    if(err){
      res.status(500).send(err);
    }
    if(!doc){
      res.status(500).send("YOUSA INSA BIGSA TROUBLE!!!");
    }
    if(doc){
      res.send(doc[0]);
    }
  } );
} );

// ## ##################
// ## POST - create ONE record (transaction)
// TEST WITH...
// http://127.0.0.1:5000/api/transactions/new
// ## ##################

/*
{
  "date":"27 Nov 2022 11:01:15",
  "total":2879.97,
  "items":[
    {"ordinal":0,"id":"TjwVu60WzlTXWt52","name":"Scooby Snacks (Box of 50)","price":9.99,"quantity":3},
    {"ordinal":1,"id":"QiV5n0IHgjB7jFid","name":"Microsoft Surface Pro 2017 Tablet","price":950,"quantity":3}
  ],
  "tax":129.6
}
*/

transactionsRouter.post( "/new", (req,res) => {
  
  var newTransaction = req.body;  

  transactionsDB.insert( newTransaction, (err,transaction) => {
    if(err){
      res.status(500).send(err);
    }
    else {
      console.log("\n transaction", transaction );
      res.sendStatus(200);
      Inventory.decrementInventory(transaction.items); // prolly correct!!!
    }
  } );
  
} );


// ## ##################
// ## DELETE - delete ONE record (product) - this is MINE for ADMIN purposes!!!
// http://127.0.0.1:5000/api/transactions/:transactionId
// ## ##################
transactionsRouter.delete( "/:transactionId", (req,res) => {
  var queryPm = { _id: req.params.transactionId };
  var optionsPm = {
    multi: false  // default false -- allows the removal of multiple documents
  };
  var callbackPm = (err, numRemoved) => {
    console.log( "err", err ); // null
    console.log( "numRemoved", numRemoved ); // 1
    if(err){
      res.status(500).send(err);
    }
    if(err === null && numRemoved === 0){
      res.status(500).send("YOUSA INSA BIGSA TROUBLE!!!");
    }
    if( numRemoved >= 1){
      res.sendStatus(200); // OK
    }
  };
  transactionsDB.remove(queryPm, optionsPm, callbackPm);
} );

module.exports = transactionsRouter;