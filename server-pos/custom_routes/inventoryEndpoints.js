const express = require("express");
const inventoryRouter = express.Router();
var Datastore = require("nedb");
const async = require("async");
inventoryRouter.use( express.json() );

// Create DB
var inventoryDB = new Datastore( {
  filename: "./custom_db/databases/inventory.db",
  autoload: true
} );

// ## ##################
// ## GET inventory
// http://127.0.0.1:5000/api/inventory
// ## ##################
inventoryRouter.get( "/", (req,res) => {  
  res.send("Inventory API");
} );

// ## ##################
// ## GET a product from inventory by productId - product
// http://127.0.0.1:5000/api/inventory/product/:productId
// e.g. -- http://127.0.0.1:5000/api/inventory/product/T4CP2M48OeFttq0g
// ## ##################

inventoryRouter.get( "/product/:productId", (req,res) => {
  
  if( !req.params.productId ){
    res.status(500).send("ID field is required");
  }
  else {
    
    var queryPm = { _id: req.params.productId };

    var callbackPm = (err, returnedDocument) => {
      
      console.log("err", err );
      console.log("returnedDocument", returnedDocument); 

      if(err){
        res.status(500).send(err);
      }
      if( err === null && returnedDocument === null ){
        res.status(500).send("YOUSA INSA BIGSA TROUBLE!!!");
      }
      if(returnedDocument !== null){
        res.send(returnedDocument);
      }
    };

    inventoryDB.findOne( queryPm, callbackPm );

  }  
} );

// ## ##################
// ## GET all records - product
// http://127.0.0.1:5000/api/inventory/products
// ## ##################
inventoryRouter.get( "/products", (req,res) => {
  
  inventoryDB.find( {}, (err, docs) => {
    if(err){
      res.status(500).send(err);
    }
    res.send(docs);
  } );
} );

// ## ##################
// ## POST - create ONE record (product) - 
// http://127.0.0.1:5000/api/inventory/product
// ## ##################

/*
{
  "name": "Scrappy Doo's Flea Collar (Set of 5)",
  "price": 19.99,
  "quantity": 10,
}
*/

inventoryRouter.post( "/product", (req,res) => {
  
  var newProduct = req.body;

  inventoryDB.insert( newProduct, (err, product) => {
    if(err) {
      res.status(500).send(err);
    }
    else {
      console.log("\n product" , product );
      res.send(product);
    }
  } );
} );

// ## ##################
// ## DELETE - delete ONE record (product) - 
// http://127.0.0.1:5000/api/inventory/product/:productId
// ## ##################
inventoryRouter.delete( "/product/:productId", (req,res) => {
  
  var queryPm = { _id: req.params.productId };
  var optionsPm = {
    multi: false  // default false -- allows the removal of multiple documents
  };
  var callbackPm = (err, numRemoved) => {
    console.log( "err", err ); // null
    console.log( "numRemoved", numRemoved ); // 1
    if(err){
      res.status(500).send(err);
    }
    if(err === null && numRemoved === 0){
      res.status(500).send("YOUSA INSA BIGSA TROUBLE!!!");
    }
    if( numRemoved >= 1){
      res.sendStatus(200); // OK
    }
  };

  inventoryDB.remove(queryPm, optionsPm, callbackPm);
} );

// ## ##################
// ## UPDATE - update ONE record (product)
// http://127.0.0.1:5000/api/inventory/product
// ## ##################

/*
{ 
  name: "Shaggy's Deluxe Quadruple Stack Hoagie", 
  price: 25.99, 
  quantity: 10, 
  _id: 'T4CP2M48OeFttq0g' 
}

{
  "name":"Scooby Snacks (Box of 50)",
  "price": 9.99,
  "quantity": 15,
  "_id":"TjwVu60WzlTXWt52"
}
*/

inventoryRouter.put( "/product", (req,res) => {
  
  var productId = req.body._id;

  var queryPm = { _id: productId };
  var updatePm = req.body;
  var optionsPm = {
    multi: false, // default false -- modify several documents
    upsert: false, // default false -- insert a new document if nothing matches
    // upsert: true, // default false -- insert a new document if nothing matches
    returnUpdatedDocs: true  // default false -- but if true and not an upsert, will return updated document array matched by the find query
  };
  
  var callbackPm = ( err, numAffected, affectedDocuments, upsert ) => {
    
    console.log( "err", err ); // null
    console.log( "numAffected", numAffected ); // 1
    
    console.log("\n affectedDocuments" , affectedDocuments ); 
    // {"name":"Scrappy Doo's Flea Collar (Set of 5)","price":19.99,"quantity":2,"_id":"j9gJBVoK9N8cdZxt"}

    // For an upsert, affectedDocuments contains the inserted document and the upsert flag is set to true.    
    // For a standard update with returnUpdatedDocs flag set to false, affectedDocuments is not set.
    // For a standard update with returnUpdatedDocs flag set to true and multi to false, affectedDocuments is the updated document.
    // For a standard update with returnUpdatedDocs flag set to true and multi to true, affectedDocuments is the array of updated documents.

    console.log( "upsert", upsert ); // return undefined if optionsPm.upsert set to false OR true if optionsPm.upsert set to true

    if(err){
      res.status(500).send(err);
    }
    else {
      res.sendStatus(200); // OK
    }
  };

  inventoryDB.update(queryPm, updatePm, optionsPm, callbackPm);

} );

// ## ##################
// ## custom method
// ## ##################
inventoryRouter.decrementInventory = function(arrItems){
  
  // console.log( "arrItems", arrItems );

  async.forEachOf( 
    
    arrItems, 

    (purchasedItem, idx, callback) => {
      
      // console.log( "purchasedItem", purchasedItem );
      // console.log( "idx", idx );
      // console.log( "callback", callback );

      try {
        var queryPm = { _id: purchasedItem.id }; // console.log( "queryPm", queryPm );

        var callbackPm = (err, itemInStock) => {          
          // console.log("err", err );
          // console.log("itemInStock", itemInStock); 
          
          if(err){
            return callback(err);
          }
          
          if( err === null && itemInStock === null ){
            return callback(err);
          }
          
          if(itemInStock !== null){
            
            // res.send(itemInStock);
            // console.log("YIPPEE - In progress " + idx );
            // console.log("YIPPEE - In progress " + purchasedItem );
            // console.log("itemInStock", itemInStock);
            // console.log("YIPPEE - purchasedItem.quantity (PURCHASED ITEM) " + purchasedItem.quantity );
            // console.log("itemInStock.quantity (NUMBER OF ITEMS IN STOCK)", itemInStock.quantity); 

            var updatedQuantity = itemInStock.quantity - parseInt( purchasedItem.quantity );

            var updateQueryPm = { _id: itemInStock._id };
            var updatePm = {
              $set : {
                ...itemInStock,
                quantity: updatedQuantity
              }
            };
            
            var updateOptionsPm = { multi: false,  upsert: false, returnUpdatedDocs: true };

            var updateCbPm = ( err, numAffected, affectedDocuments, upsert ) => {
              // console.log( "err", err ); // null
              // console.log( "numAffected", numAffected ); // 1
              // console.log("\n affectedDocuments" , affectedDocuments );
              // console.log( "upsert", upsert );
              if(err){
                return callback(err);
              }
              else {
                console.log( "############ DONEZO ############ " );
                callback();
              }
            };
            
            inventoryDB.update(updateQueryPm, updatePm, updateOptionsPm, updateCbPm);

          }
        };

        inventoryDB.findOne( queryPm, callbackPm );
        
      }
      catch(e){
        console.log( "try/catch e", e );
        return callback(e);
      }
    } , 
    err => {
      if (err) {
        console.error(err.message);
      }
    }
  );
};

module.exports = inventoryRouter;