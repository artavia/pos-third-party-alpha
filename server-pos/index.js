// =============================================
// BOILERPLATE
const express = require("express");
const app = express();
const http = require("node:http"); 

var liveCart = [];
const { Server : SocketIOServer } = require("socket.io"); 
const cors = require("cors");

// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}
const { /* NODE_ENV , */ PORT , SERVERHOSTNAME , } = process.env;

// =============================================
// ROUTER SETUP
const transactionsRouter = require("./custom_routes/transactionsEndpoints");
const inventoryRouter = require("./custom_routes/inventoryEndpoints");

// =============================================
// APP SETUP - USE PARAMS - FINER DETAILS
var corsOptions = {
  
  // "origin": "*" , // The default configuration is the equivalent of 1/4:
  "origin": [`http://localhost:3000`, `http://127.0.0.1:3000`, `http://localhost:5000`, `http://127.0.0.1:5000`] , // YEPPERS!

  "optionsSuccessStatus": 204 ,  // The default configuration is the equivalent of 2/4:  
  // "optionsSuccessStatus": 200 , // some legacy browsers (IE11, various SmartTVs) choke on 204

  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE" , // The default configuration is the equivalent of 3/4:

  "preflightContinue": false , // The default configuration is the equivalent of 4/4:

}; 

// app.use( cors() );
app.use( cors( corsOptions ) ); 
app.use( express.json() );
app.use( express.urlencoded( { extended: true } ) );

// =============================================
// API rules

// Log the request
app.use( ( req, res, next) => {
  console.info(`METHOD: [${req.method}] - URL: [ ${req.url} ] - IP: [ ${req.socket.remoteAddress } ]`);
  res.on( 'finish', () => {
    console.info(`METHOD: [${req.method}] - URL: [ ${req.url} ] - STATUS: [ ${ res.statusCode } ] - IP: [ ${req.socket.remoteAddress } ]`);
  } );
  next();
} );

// =============================================
// Server variables setup
const httpServer = http.createServer( app );

// =============================================

// websocket variables setup 
const serversocketconfig = {
  
  cookie: false , 

  cors: {     
    
    allowedHeaders: [
      "my-custom-header", // custom
      "Origin", "Content-type", "Accept", // websockets
      "X-Requested-With" , "Authorization" , // Authorization 
      "Access-Control-Allow-Origin" , // THIS IS NEW
    ] , 
  } ,
  pingInterval: 10000 ,
  pingTimeout: 5000 ,
  serveClient: false ,
};

const io = new SocketIOServer( httpServer, serversocketconfig );

// =============================================
// APPLY THE ROUTES TO THE APP

// Index page
app.get('/', ( req, res, next ) => {
  // res.send("Real time POINT-OF-SALE web app is running, baby!");
  return res.json( { message: "This is Express!!!" } );
} );

// Healthcheck
app.get('/ping', ( req, res, next ) => {
  return res.json( { konichiwa: "WHIRLLED WORLD!!!" } );
} );

app.use( "/api/transactions", transactionsRouter ); // e.g. -- "/api/transactions" || ""
app.use( "/api/inventory", inventoryRouter);        // e.g. -- "/api/inventory" || ""

// =============================================// Websocket logic 


io.on( "connection" , (ws) => {

  console.log(`SocketIOServer instance - User Connected...: ${ws.id}`);

  // ##### START UNIT TESTED SECTION #####
  // upon page load, give user current cart... On page load, make client update live cart 
  ws.on( "live-cart-page-loaded", () => { 
    // console.log( "live-cart-page-loaded - liveCart", liveCart ); 
    ws.emit("update-live-cart-display", liveCart );
  } );
  
  // fires when the cart data is updated by POS
  ws.on( "update-live-cart", (cartData) => {

    // console.log( "update-live-cart - cartData", cartData );
    
    liveCart = cartData; // keep track of live cart
    console.log( "update-live-cart - liveCart", liveCart ); 

    // broadcast updated live cart to all websocket clients    
    ws.broadcast.emit("update-live-cart-display", liveCart ); // USE FOR NORMAL LIVE PURPOSES

  } );

  ws.on("disconnect", (reason) => {
    console.log("disconnect reason: " , reason );
    console.log(`SocketIOServer instance - User disconnected...: ${ws.id}`); 
  } );
  // ##### END UNIT TESTED SECTION #####

} );

// =============================================
// BACKEND SERVER INSTANTIATION

httpServer.listen( PORT, SERVERHOSTNAME, null, () => {
  console.log( `Server is running at http://${SERVERHOSTNAME}:${PORT} .`);
} ); // PORT, hostname, backlog (null), cb 
