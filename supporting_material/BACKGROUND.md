# Background

## What is the purpose behind all of this?
For my side-business which is my only income at the moment, I have a back-end project. I have an express delivery business. I am a team of one! My bicycle is my transport while MySQL serves as the backbone of my business. The work is **unpublished**.  If my bigger goal were referred to as the main course this work, specifically, would be considered a(n) hors d'oeuvres (not pronounced horse-doovers)!

## Small history on just &quot;the initial database phase&quot;
But, it organizes several aspects of the business way better than LibreOffice (where it started) every could have. First, I fleshed out the databases between **July 25, 2021 to August 9, 2021**. It was a matter of extrapolating what I needed from LibreOffice Calc spreadsheets that I had already begun to use for tabulating everything. As you can imagine, that became complicated real quick.

The database called **Jericho** has tables that have relationships with one another. Without divulging any more than I have to, there are tables such as:
  - &quot;users&quot;, &quot;entrees&quot;, &quot;carryout_costs&quot;, &quot;recipes&quot;, &quot;wholesale_costs&quot;, &quot;packaging&quot;, &quot;measurements&quot;, &quot;ingredients&quot;, &quot;contact_types&quot;, &quot;contacts&quot;.

A table to track &quot;sales&quot; is conspicuously missing. The back-end (administrative side) does everything except handle and/or interact with sales. It calculates costs, it factors in markup price, taxes and tracks ingredients as well as suppliers and customers. Presently, all sales have been documented in **LibreOffice**, but I had to go back and plan on how to integrate the **sales data** in an manner ideal **for use with MySQL**. 

For now, I interact with my back-end tracking app locally and that is enough for the moment. I **might** publish another version to **git** but with dummy data in order to protect proprietary trade secrets and personal data.

## Smaller history on just &quot;the unpublished project&quot;
Basically, I worked on **the initial, unpublished phase of the project** from April 7, 2022 to May 19, 2022. This is the thing I described that interacts with all of my tables except &quot;sales&quot;.

## POS
Hence, shopping-cart and/or point-of-sale apps became a hot item of interest to me. This project is designated as **alpha**. It's my baby! **The Lord God himself** permitted me to build this out **without anybody's help**. This repo is warm-up. It is an iteration. The MySQL version of this project is another iteration. 

**Beta** is currently unprocessed but it deals with offline first. That is a subject with which I already have experience. In the not so distant future, Lord permitting, I will **continuously deploy** IndexedDB functionalities into the current project (yet to be published to the cloud) at that point in time. 

The final iteration of this project (whatever greek letter it is called) **will fit on top** of the original, so far, unpublished project.

## This project opened the doors to other subjects
I think that euphemism I am looking for is **rabbit's hole**. After embarking on this journey, I unknowingly was heading for a trip down the **rabbit's hole**!

To make a long story short, I will likely open another repo that contains micro-lessons worthy of publication but do not necessarily warrant their own individual repository. Let me illustrate some examples:

 - First, I have an accompanying project but it uses **MySQL** and not NeDB;
 - Next, I wanted to use **UUID** so I did. 
 - Then, NeDB, in this example, blurted out a string for each transaction that included fields such as *id*, *date*, *total*, *tax* and *items*. The *items* field is a stringified object constituted of several details corresponding to each individual product sold to make up that transaction. Therefore, the **LONGTEXT** data type in the context of MySQL became a thing. I later found my sea legs working with a LAMP stack in order to get acclimated with *LONGTEXT*. 
 - In lateral terms and in an indirectly related fashion, I played around with **MongoDB transactions**. So I was already considering how to mimic this behavior but with MySQL. Thus, I have yet to learn more about called *MySQL Stored Procedures*.
 - React has many different types of methods and/or hooks called **refs**. And, the original project used class-based components (something I abandoned **years** ago) but the hooks I used such as **useRef**, **createRef** and **forwardRef** apply only function-based components. These were life-savers since they facilitate code organization and readability. They permit the developer to break up the code into smaller pieces and that is something that sells itself!
