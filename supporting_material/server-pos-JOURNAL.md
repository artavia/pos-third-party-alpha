# Journal
This is a journal which details the evolution of the back-end with perhaps some small amount of exceptions.
  
  - September 25, 2022 - Start building Express back-end and Socket.io in earnest; worked on said items between **1:15pm** and **2:15pm**. 

  - September 25, 2022 - Continue with Express back-end and Socket.io; worked on said items between **6:30pm** and **8:00pm**. 

  - September 26, 2022 - Continue with Express back-end; worked on said item between **7:20am** and **8:45am**. Partially finished the inventory endpoints; finished the transactions endpoints. Familiarized self with NEDb (AnyDB???) and async convenience package.

  - September 26, 2022 - Continue with Express back-end; worked on said item between **12:00pm** and **12:30pm**. Finish the inventory endpoints. 

  - September 26, 2022 - Continue with Express back-end; worked on said item between **1:30pm** and **2:30pm**. Re-educate myself with ExpressJS req.query and res.header (aka res.set( {"my-key": val} )). Begin testing endpoints with Postman prior to building up the front end. ~~ I cannot begin testing endpoints with Postman prior to building up the front end because I have no models or dummy data to work with.~~ Hence, I am moving confident that the backend is complete.

  - September 27, 2022 - Continue with Express back-end and NEDB db samples; worked on said item between **4:30pm** and **6:15pm**. Located two sample DBs in text format. They are essentially javascript objects written to text files. Each of the two have their own niceties but I should be able to work out what the fields are for each of the two databases. Therefore, I am now able to utilize Postman and confirm the back end functionality with each the ExpressJS endpoints but not the Socket.io just quite yet. 

  - September 27, 2022 - Continue with Express back-end and NEDB db samples; worked on said item between **6:15pm** and **7:00pm**. I had to round up a reasonable amount of dummy data and make some changes and work out any previous kinks in the back end that I personally had introduced. I only had time to build the inventory. Later tonight or tomorrow morning, I will look for ways to use **date-fns** instead of **moment.js**, plus, build up the Transactions Database.

  - September 28, 2022 - Continue with Express back-end and NEDB db samples; worked on said item between **10:00am** and **10:30am**. Make a few small changes for posterity's sake to the Inventory Database endpoints.

  - September 28, 2022 - Continue with Express back-end and NEDB db samples and React front-end; worked on said item between **1:00pm** and **2:15pm**. Make small changes to the Transactions Database endpoints before continuing with the larger tasks, then, I am moving on. Make some additions to package.json content on the front-end. Moving on, too.

  - September 28, 2022 - Continue with Express back-end and NEDB db samples; worked on said item between **2:45pm** and **5:00pm**. Continue with the Transactions Database. Inserted some transactions dummy data but the async npm package was obsolete, hence, the **decrementInventory()** router method had to be totally re-expressed.

  - September 28, 2022 - Continue with Express back-end and NEDB db samples; worked on said item between **5:15pm** and **6:30pm**. Successfully finished the **decrementInventory()** method. Foreseeably, I can focus entirely on the front-end given that I have no reason to suspect anything is amiss with any of the previously established endpoints. Yippee!

  - October 2, 2022 - Modify the Express back-end and NEDB db samples; worked on said items between **3:00pm** and **3:15pm**. Add an extra field to the transaction post new record endpoint in order to include an **ordinal** field to supplement the **id** field which I have used to refer to the **id** field from the product table/database in terms of NEDB. 

  - October 9, 2022 - Continue with Express back-end focusing on SocketIO instantiation and endpoints; worked on said item between **9:00am** and **9:45am**. Untangled the previous mess originally cited in the example code. I will slowly uncomment other pertinent endpoints once I am more confident in the anticipated results.

  - May 11, 2023 - I had to graft some code from the MySQL version iteration. Specifically, the line in the express server at **index.js** that innocuously declares **cors()** was updated to include a configuration object. This permitted proper communication between two browser sessions in the client when viewing especially in **chromium**. Although unlikely in the event that the front-end of this project is **built** after running **npm run build**, then, helmet will have to be installed. This concept was explored and bore good fruit in the MySQL version of the project but **it would be overkill** in this particular project.