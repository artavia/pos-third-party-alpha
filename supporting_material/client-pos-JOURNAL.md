# Journal
This is a journal which details the evolution of the front-end with perhaps some small amount of exceptions.
  
  - September 26, 2022 - Start building React front-end in earnest; worked on said item between **5:15pm** and **7:15pm**. 

  - September 26, 2022 - Continue building the React front-end; worked on said item between **8:00pm** and **9:45pm**. The work consists of using class-based components before eventually switching back over to functional components coupled with useState.

  - September 27, 2022 - Continue building the React front-end; worked on said item between **7:45am** and **8:15am**. Begin by addressing minor styling issue. The issue was with RR v6 and the accompanying warning stating **"Warning: Functions are not valid as a React child. This may happen if you return a Component instead of &laquo;Component /> from render."** The fact is that I had mistakenly applied non-rendered elements in some areas (e.g. - **Pos** instead of **&laquo;Pos />**).

  - September 27, 2022 - Continue building the React front-end; worked on said item between **8:30am** and **9:15am**. Track down slippage when using **@floating-ui** (in fact, visit home page to clear up appropriate React usage). Then, start building out the different components.

  - September 27, 2022 - Full stop and hard brake. I had a suspicion that the CRA project was not correctly created, hence, I will back up this version, then, re-create the project from the floor up. Even if I use yarn and not npm, I need to ensure that all was created correctly. This work was performed between **9:15am** and **12:00pm**. Now I can continue with the rest of the project. The problem was more complicated than anticipated.

  - September 27, 2022 - Continue building the React front-end; worked on said item between **12:15pm** and **12:30pm**. Repatriate all of the previous content and code to their respective documents and locations. 

  - September 27, 2022 - Continue building the React front-end; worked on said item between **1:15pm** and **1:30pm**. Continue tracking **@floating-ui** issues. Actually, it is an **es-lint** issue, thus, to cure it one must generically apply **// eslint-disable-next-line** at the top of the offending page. Next, start building out the different components. 

  - September 27, 2022 - Continue building the React front-end; worked on said item between **2:45pm** and **3:15pm**.

  - September 28, 2022 - Continue with Express back-end and NEDB db samples and React front-end; worked on said item between **1:00pm** and **2:15pm**. Make small changes to the Transactions Database endpoints before continuing with the larger tasks, then, I am moving on. Make some additions to package.json content on the front-end. Moving on, too.

  - September 29, 2022 - Continue building the React front-end; worked on said item between **8:00am** and **9:30am**. Built the **LivePos** page and start to build the **Pos** page. Also, removed **socket.io** but added the **socket.io-client** package instead.

  - September 29, 2022 - Continue building the React front-end; worked on said item between **10:30am** and **11:15am**. Continued with building the **Pos** page in anticipation of viewing some live data. I thought over the axios instances and moment instances scattered throughout the front-end. To address the former, I began to build a service that will house all of the axios declarations under one roof. With the latter, I am considering using plain vanilla JS Date object or date-fns instead.

  - September 29, 2022 - Continue building the React front-end; worked on said item between **8:15pm** and **9:30pm**. Continued with building the **Pos** page. Located and matched some of the antiquated Bootstrap css classes with their newer modern counterparts (e.g. -- pull-left became float-left which is now float-start). Imported a current copy of a Modal expressed with useState(). Made some modifications in order to accommodate class-based state instead.
  
  - September 30, 2022 - Continue building the React front-end; worked on said item between **3:45pm** and **4:45pm**. Continued with building the **Pos** page.

  - September 30, 2022 - Continue building the React front-end; worked on said item between **6:45pm** and **7:45pm**. Continued with building the **Pos** page. My eyes are getting crossed with keeping track of the innumerable quantity of modals.  I will pick up with the second modal after likely making further distinctions in the previous innocuous attributes and state names that were applied to the first modal. I am not satisfied yet with working on the second modal when the first modal is not quite up to snuff.

  - September 30, 2022 - Continue building the React front-end; worked on said item between **8:45pm** and **9:45pm**. Continued with building the **Pos** page.

  - September 30, 2022 - Continue building the React front-end; worked on said item between **10:00pm** and **10:45pm**. Continued with building the **Pos** page. Completed the addItemModal. 

  - October 1, 2022 - Continue building the React front-end; worked on said item between **6:15am** and **6:30am**. Continued with building the **Pos** page. Started to bring to to fruition each of the following functionalities and their accompanying modals: renderLivePos, renderReceipt and renderAmountDue.

  - October 1, 2022 - Continue building the React front-end; worked on said item between **12:00pm** and **12:15pm**. Continued with building the **Pos** page. Finished the three (3) above-mentioned functions. 
  
  - October 2, 2022 - Continue building the React front-end; worked on said item between **9:00am** and **10:15am**. Continued with building the **Pos** page. Focus on the following items:
    - Address the compilation warning associated with the only map function in **Pos.js**.
    - Continue next with the **handleChange** function, then, look at **handleSubmit**, **handleName**, **handlePrice**, **handlePayment**, **handleChange**, **handleCheckOut**, **handleSaveToDB**.

  - October 2, 2022 - Continue building the React front-end; worked on said item between **10:15am** and **10:15am**. I have an observation that needs to come to light: I need to add an extra field to the transaction endpoint in order to include an **ordinal** field to supplement the **id** field which I have used to refer to the **id** field from the product table/database in terms of NEDB. Therefore, I backed-up the whole project, then, found and replaced and/or supplemented new **ordinal** fields in addition to the pre-existing **id** fields within the context of the front-end **but only after** I had inserted this new **proposed** field to the back-end.

  - October 9, 2022 - Continue with the front-end focusing on SocketIOClient instantiation and endpoints; worked on said item between **10:30am** and **11:45am**. Created a new class-based test route to ensure that the handshake worked correctly with some sample data before continuing. 

  - October 13, 2022 - Continue with the front-end focusing on NEW pages; worked on said item between **3:15am** and **4:45am**. Embarked on producing the **Inventory and Product pages** before making the anticipated major adjustment to the **Pos page**. So far, only the **Product** page is 90% finished until further testing is performed.

  - October 13, 2022 - Continue with the front-end focusing on NEW pages; worked on said item(s) between **5:45am** and **6:15am**. Addressing the low-hanging fruit such as **RecentTransactions**, **TransactionDetail**, and **LiveTransactions**.

  - October 16, 2022 - Continue with the front-end focusing on NEW pages; worked on said item(s) between **6:30am** and **9:15am**. Skipping over to the **Inventory page** before returning to conclude with the **Product** page.

  - October 16, 2022 - Continue with the front-end focusing on NEW pages; worked on said item(s) between **7:30pm** and **8:15pm**. Skipping over to the **Inventory page** before returning to conclude with the **Product** page. **These two pages are finalized.** Both pages were changed from class components to dumb components. Now everything contained within has been modularized and they **work better than expected**!

  - November 18, 2022 - Continue with the front-end focusing on NEW pages; worked on said item(s) between **5:15am** and **5:45am**. In the process of changing from class to dumb components, therefore, revisiting the low-hanging fruit such as **RecentTransactions**, **TransactionDetail**, and **LiveTransactions**.

  - November 19, 2022 - Continue with the front-end; **Minutes or hours are not available**. Details are yet TBD. Assessed **the Pos page** and got re-acquainted with it. It is too large and eventually needs to be split similar to the inventor page that I processed previously. But for now I would be happy to get it fully running. There is an order where one bug precipitates the next and these bugs are not limited to only one function. It will get interesting

  - November 20, 2022 - Continue with the front-end; **Minutes or hours are not available**. In terms of **Pos**, several **createRef()** instances were utilized to take the place of previous **state** instances that seemed gratuitous. And, some disabled functionality was applied to make the **Add Item form** more user friendly. And, with the **Add Item form**, a &lt;select&gt; element was added which is now the **star of the show** at least in term of the function called **handleSubmit()** (which I renamed to **handleNewItemSubmission()**).

  - November 21, 2022 - Continue with the front-end; **Minutes or hours are not available**. In terms of **Pos**, with **handleNewItemSubmission()** completed the day before, the focus was shifted to the checkout button functionalities in **handleCheckOut()**.

  - November 22, 2022 - Continue with the front-end; **Minutes or hours are not available**. In terms of **Pos**, continue with **handleCheckOut()**. Locate the different deficient areas that are scattered throughout. Here is a short list:
    - **this.state.total** right at the top immediately next to the **handleCheckOut()** button with the shopping cart does not update until handleCheckOut() is launched and not when new items are gradually added or removed in the **handleNewItemSubmission()** -- this does not exist and it should;
    - There should be a tax rate constant, then, in **the LivePos component** a NEW computation consisting of **the product of tax by the 'grandtotal'** can take place ; 
    - And, **this.state.total** needs to adequately compute **the product of price by quantity** in **the LivePos component** -- these do not exist and they should;
    - And, **this.state.total** needs to adequately handle dollars and cents instead of rounding quantities altogether.

  - November 25, 2022 - Continue with the front-end; **Minutes or hours are not available**. **Pos** is 95% finished with the exception of some aesthetic details and additional preferences that are being considered. What is at issue is an **applied tax rate** property that will be applied to state. This will be necessary to compute a **total sales tax** to be applied with every "sale." Before moving on, I will likely convert this **Pos** page and split it several individual functional components. With past experience set as precedent as far as the **inventory** page is concerned, similarly the **Pos** should be easy to manage now that the difficult part is over with.

  - November 26, 2022 - Continue with the front-end; **Minutes or hours are not available**. The class-based version of **Pos** is finished and it works perfectly. It has been thoroughly tested and there are no edge cases present. Thus, **Pos** is now ready to be split into several individual functional dumb components.

  - November 27, 2022 - Continue with the front-end; **Minutes or hours are not available**. I have come to the determination that it is best that I split **any and all class-based components** into several individual functional dumb components only after they are properly working. The other remaining pages-- while smaller in size --are still just as unorganized as the previous pages I have successfully processed until now.

  - November 27, 2022 - Continue with the front-end; **Minutes or hours are not available**. Each **Transactions**, **CompleteTransactions** and **TransactionDetail** have been completed and they work perfectly. There are no edge cases present and all is in order.

  - November 27, 2022 - Only three components are left and they include **LiveCart**, **RecentTransactions** and **LiveTransactions**. Once **ALL** pages have been processed, then, any and all **Socket.IO** functionalities will be re-instated. Presently, all **Socket.IO** functionalities are disabled in order to make console testing less cluttered.

  - November 29, 2022 - **The class-based component version of the project is done and there is much room for improvement.** For starters two additional endpoints can be created in React. One endpoint can display an individual transaction and the other endpoint can display an individual product. Then, all of the server endpoints have not been utilized. There are get queries that search by a range of dates. The **LiveCart** page is interesting but it looks incomplete. And, to be honest this whole lesson is just an appetizer and a warm-up for bigger things. NEDB is very limited in its application and it is not practical for anything other than finding your bearings when exposed to Point-of-sales applications for the first time like I was just recently. Now, I can apply the lessons that were learned to a different stack where I would replace NEDB with Sequelize and MySQL because that is what I have worked on throughout the past year. The inclusion of Socket.IO definitely opened my eyes and made this experience fun. If I had a chance to get in touch with the original author I would forward him my working product because his did not work from the get go. That was frustrating but when the clouds faded away the light punched through in unstoppable fashion! Next, **I have to convert all class-based components and split them into smaller individual functional components** similar to what was accomplished with the **Inventory** and **Product** pages.

  - November 29, 2022 - I may research and address **socket.disconnect()** and **socket.disconnected()** in the context of the **componentWillUnmount** lifecycle method in the two pages where the **Socket.IO-client** is used. 

  - November 30, 2022 - For the moment all items except for **the Pos page** have been successfully converted into functional components. This is a work in progress. I had a sore throat subsequent to a visit to the dentist during the previous day, hence, I had to stop for health reasons.

  - December 1, 2022 - When permitted to concentrate without interruption, I am still working on **the Pos page**. Let the Lord's will be done (not mine) and all glory, praise, honor and blessings shall be given to God the father, God the Holy Spirit, and God the anointed son, Jesus Christ, especially if I must be on my knees as a requisite. Amen!

  - December 5, 2022 - This evening I dedicated a couple of hours to the **Pos page** and it is now 95% finished; three (3) functions had to be re-expressed before things got back on track. In order to reach this stage, I had to disable the socket functionality so that I could accurately track any progress. I still have an itchy, dry cough, hence, I will conclude this specific phase of the page in question tomorrow.

  - December 6, 2022 - This evening the **function-based Pos page** was completed; **Minutes or hours are not available**. In **both Pos and LiveCart pages** the **date-fns** library in lieu of **moment** package. And, before running out of time it occurred to me that I should spruce up **the LiveCart page**. By this I mean 1) implementing proper use of Bootstrap grid; 2) filtering certain transactions by date or range of dates or both; 3) sorting in descending order the transactions in each **the Pos and LiveCart pages**; and, 4) come up with a composite figure of total sales using Array.reduce().

  - December 7, 2022 - Researched all of the above items during the morning; **Minutes or hours are not available**. By early afternoon I was ready to implement the proposed changes mentioned in the entry above. By 8pm everything I set out to do-- minor interruptions notwithstanding --was completed. But by 9pm, I decided that **I need &lt;input /&gt; of type&eq;date** in order to utilize two unused endpoints as they relate to transactions.

  - December 8, 2022 - During the morning and prior to a scheduled medical appointment, I set out to re-arrange each the **Pos and LiveCart pages**; **Minutes or hours are not available**. This introduced unforeseen behavior, thus, restored each back to their previous LKGCs. After the appointment, I researched **&lt;input /&gt; of type&eq;date** and found that I have options. Second, upon testing I found out that two unused transaction endpoints are totally flawed and have to be re-done. These are functionalities and/or outcomes that I already worked out in the frontend with my first draft of **LiveCart**. These outcomes and heavy lifting have to be, however, more deliberate on the backend. Third, given that transactions are assigned a date value in string format, these two **flawed endpoints** need a **$where** search modifier in the search parameter so the the dates in the database are ultimately expressed as sortable numbers in unix timestamp terms subsequent to having been converted to date objects from their original string format. **All of these new observations-- once addressed --will require new sub-sections in the LiveCart page to make use of all the endpoints.** This is what I will flesh out tomorrow.

  - December 9, 2022 - During the morning, I worked on the second draft of **the LiveCart page** in the frontend and I worked on the second draft of **the transactionEndpoints page** in the backend; **Minutes or hours are not available**.

  - December 10, 2022 - I continued to focus on **the LiveCart page** in the frontend and focused on **the transactionEndpoints page** in the backend; **Minutes or hours are not available**.

  - December 11, 2022 - I finished **the LiveCart page** in the frontend and finished **the transactionEndpoints page** in the backend; **Minutes or hours are not available**. Now the project is officially finished given that I am happy with the results.

  - December 12, 2022 - I decided to flesh out both a **ViewTransaction page** and a **ViewProduct page** in the frontend both of which did not previously exist; **Minutes or hours are not available**. The project is in better shape today than it was yesterday. Also, while researching **the latest version** of ReactRouter v6, I decided to upgrade from **v6.4.2** to the latest and greatest version which today is **v6.4.5**. Despite the outcome it's for the best. I was already witnessing slippage between current documentation and the import declarations in front of me. **What could possibly go wrong?!?!?!** The hook that plausibly was **useSearchParams()** is now  **useParams()**. All of the aforementioned tasks were completed today.

  - May 11, 2023 - All of the referenced server endpoints contained in the front-end were hard-coded for use with port 5000 when in truth they (the URL references) should have remained port agnostic based on my own experience. All appropriate changes were made to reflect this.